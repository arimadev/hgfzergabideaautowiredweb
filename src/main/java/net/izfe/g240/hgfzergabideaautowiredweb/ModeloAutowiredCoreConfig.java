/*
 * Copyright (c) 2017, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g240.hgfzergabideaautowiredweb;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author inigo
 */
@Configuration
public class ModeloAutowiredCoreConfig {

  @Bean
  public CoreFacade coreFacade() {
    return new CoreFacade();
  }

  public static class CoreFacade {

  }

}
