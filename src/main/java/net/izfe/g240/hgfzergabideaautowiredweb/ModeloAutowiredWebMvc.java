/*
 * Copyright (c) 2017, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g240.hgfzergabideaautowiredweb;

import javax.annotation.PostConstruct;

import net.izfe.g240.hgfzergabideaautowiredweb.ModeloAutowiredCoreConfig.CoreFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author inigo
 */
@Configuration
public class ModeloAutowiredWebMvc {

  @Autowired
  CoreFacade coreFacade;

  public ModeloAutowiredWebMvc() {
    System.out.println("Constructor de ModeloAutowiredWebMvc");
  }

  @PostConstruct
  public void init() {
    System.out.println("Valor de coreFacade en ModeloAutowiredWebMvc" + this.coreFacade);
  }

  @Bean
  public static PropertyPlaceholderConfigurer placeholderConfigurer() {
    return new PropertyPlaceholderConfigurer();
  }

}
